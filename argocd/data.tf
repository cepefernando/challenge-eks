// get the remote state data for eks
data "terraform_remote_state" "eks" {
  backend = "s3"

  config = {
    bucket         = "andres-terraform"
    key            = "argocd.json"
    region         = "us-east-1"
    dynamodb_table = "andres-terraform"
  }
}
