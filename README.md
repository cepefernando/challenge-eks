# Setup EKS and ArgoCD
EKS Deployment with ArgoCD with Terraform.

### CI Environment Variables

These Environment Variables are needed for the pipeline when
runnig Terraform commands.

* `AWS_ACCESS_KEY` - AWS Access Key for AWS Access
* `AWS_SECRET_KEY` - AWS Access Key for AWS Access
* `AWS_DEFAULT_REGION` - AWS region where the S3 bucket is located

### Local Environment Variables

These Environment Variables are needed for the pipeline when
runnig Terraform commands.

* `AWS_DEFAULT_REGION` - AWS region to create the resources

* `AWS_ACCESS_KEY_ID` - Access Key ID to be used by the pipeline
to authenticate with your AWS Account

* `AWS_SECRET_ACCESS_KEY` - Secret Access Key to authenticate with your AWS Account

### Pre setup
You need to do these bootstrapping bits.

* Create an `S3 Bucket` and `Amazon DynamoDB` for you to store terraform remote state and state locking.
* Create a `Hosted zone` in `Route53` with a public domain. In this case, DNS Delegation was used on an existing domain.

### EKS Terraform Variables Values
Check `sample.tfvars` file in `eks` directory and update accordingly especially `dns_hosted_zone`, `admin_users` and `developer_users`.


### Running Locally

You need to first apply the `eks` before applying `argocd`.

#### EKS
Check into the folder
```bash
cd eks
```

##### Terraform Commands

```bash
# Initialize Terraform
terraform init -backend-config=backend.tfvars

# Generate Plan
terraform plan -var-file=sample.tfvars

# Apply the Plan
terraform apply

# Connect to Cluster
aws eks update-kubeconfig --name $CLUSTER_NAME --region us-east-1 
```


#### ArgoCD
Check into the folder
```bash
cd argocd
```

##### Terraform Commands

```bash
# Initialize Terraform
terraform init -backend-config=backend.tfvars

# Generate Plan
terraform plan

# Apply the Plan
terraform apply

# Access ArgoCD locally
 kubectl port-forward svc/argocd-argocd-demo-server -n argocd 8080:443
```

## Terraform Modules
Leveraging the use of official AWS Modules

- https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/19.13.0
- https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/4.0.2
