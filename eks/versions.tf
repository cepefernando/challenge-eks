terraform {
  required_version = "1.4"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=4.50.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.5.1"
    }
    time = {
      source  = "hashicorp/time"
      version = "0.9.1"
    }
  }

  backend "s3" {
    bucket         = "andres-terraform"
    key            = "argocd.json"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "andres-terraform"
  }

}
