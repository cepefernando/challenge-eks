data "aws_availability_zones" "available_azs" {
  state = "available"
}

data "aws_caller_identity" "current" {}

data "aws_route53_zone" "hosted_zone" {
  name = var.dns_hosted_zone
}

data "aws_eks_cluster_auth" "cluster" {
  name = var.cluster_name
}
